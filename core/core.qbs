import qbs

DynamicLibrary {
    name: "core"
    Depends {
        name: "buildsettings"
    }
    Depends {
        name: "util"
    }
    Depends {
        name: "Qt"
        submodules: ["core"]
    }
    cpp.includePaths: product.sourceDirectory + "/.."
    Export {
        Depends {
            name: "cpp"
        }
        Depends {
            name: "util"
        }
        Depends {
            name: "Qt"
            submodules: ["core"]
        }
        cpp.includePaths: product.sourceDirectory + "/.."
    }
    Group {
        fileTagsFilter: product.type
        qbs.install: true
    }
}
