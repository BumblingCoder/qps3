#include "patternfactory.h"

#include "core/session.h"

namespace qPs3 {
namespace core {

Pattern CreatePattern(Session& session) {
  Pattern pattern;
  pattern.id._id = session.nextPatternId;
  pattern.name = QObject::tr("Pattern %1").arg(session.nextPatternId);
  session.nextPatternId++;

  for(auto track : session.song.tracks) {
    PatternTrack pt;
    pt.noteColumns.resize(track.noteColumns);
    pt.fxColumns.resize(track.fxColumns);
    for(auto& a : pt.noteColumns) { a.notes.resize(session.defaultPatternLines); }
    for(auto& a : pt.fxColumns) { a.fx.resize(session.defaultPatternLines); }
    pattern.items.push_back(pt);
  }
  return pattern;
}

} // namespace core
} // namespace qPs3
