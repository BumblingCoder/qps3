#include "note.h"

#include <QObject>
#include <QString>

namespace qPs3 {

namespace core {

QString toString(Note n) {
  switch(n) {
    case Note::Empty: return "..";
    case Note::NoteOff: return QObject::tr("Off");
    case Note::A0: return "A-0";
    case Note::As0: return "A#-0";
    case Note::B0: return "B-0";
    case Note::C0: return "C-0";
    case Note::Cs0: return "C#-0";
    case Note::D0: return "D-0";
    case Note::Ds0: return "D#-0";
    case Note::E0: return "E-0";
    case Note::F0: return "F-0";
    case Note::Fs0: return "F#-0";
    case Note::G0: return "G-0";
    case Note::Gs0: return "G#-0";
    case Note::A1: return "A-1";
    case Note::As1: return "A#-1";
    case Note::B1: return "B-1";
    case Note::C1: return "C-1";
    case Note::Cs1: return "C#-1";
    case Note::D1: return "D-1";
    case Note::Ds1: return "D#-1";
    case Note::E1: return "E-1";
    case Note::F1: return "F-1";
    case Note::Fs1: return "F#-1";
    case Note::G1: return "G-1";
    case Note::Gs1: return "G#-1";
    case Note::A2: return "A-2";
    case Note::As2: return "A#-2";
    case Note::B2: return "B-2";
    case Note::C2: return "C-2";
    case Note::Cs2: return "C#-2";
    case Note::D2: return "D-2";
    case Note::Ds2: return "D#-2";
    case Note::E2: return "E-2";
    case Note::F2: return "F-2";
    case Note::Fs2: return "F#-2";
    case Note::G2: return "G-2";
    case Note::Gs2: return "G#-2";
    case Note::A3: return "A-3";
    case Note::As3: return "A#-3";
    case Note::B3: return "B-3";
    case Note::C3: return "C-3";
    case Note::Cs3: return "C#-3";
    case Note::D3: return "D-3";
    case Note::Ds3: return "D#-3";
    case Note::E3: return "E-3";
    case Note::F3: return "F-3";
    case Note::Fs3: return "F#-3";
    case Note::G3: return "G-3";
    case Note::Gs3: return "G#-3";
    case Note::A4: return "A-4";
    case Note::As4: return "A#-4";
    case Note::B4: return "B-4";
    case Note::C4: return "C-4";
    case Note::Cs4: return "C#-4";
    case Note::D4: return "D-4";
    case Note::Ds4: return "D#-4";
    case Note::E4: return "E-4";
    case Note::F4: return "F-4";
    case Note::Fs4: return "F#-4";
    case Note::G4: return "G-4";
    case Note::Gs4: return "G#-4";
    case Note::A5: return "A-5";
    case Note::As5: return "A#-5";
    case Note::B5: return "B-5";
    case Note::C5: return "C-5";
    case Note::Cs5: return "C#-5";
    case Note::D5: return "D-5";
    case Note::Ds5: return "D#-5";
    case Note::E5: return "E-5";
    case Note::F5: return "F-5";
    case Note::Fs5: return "F#-5";
    case Note::G5: return "G-5";
    case Note::Gs5: return "G#-5";
    case Note::A6: return "A-6";
    case Note::As6: return "A#-6";
    case Note::B6: return "B-6";
    case Note::C6: return "C-6";
    case Note::Cs6: return "C#-6";
    case Note::D6: return "D-6";
    case Note::Ds6: return "D#-6";
    case Note::E6: return "E-6";
    case Note::F6: return "F-6";
    case Note::Fs6: return "F#-6";
    case Note::G6: return "G-6";
    case Note::Gs6: return "G#-6";
    case Note::A7: return "A-7";
    case Note::As7: return "A#-7";
    case Note::B7: return "B-7";
    case Note::C7: return "C-7";
    case Note::Cs7: return "C#-7";
    case Note::D7: return "D-7";
    case Note::Ds7: return "D#-7";
    case Note::E7: return "E-7";
    case Note::F7: return "F-7";
    case Note::Fs7: return "F#-7";
    case Note::G7: return "G-7";
    case Note::Gs7: return "G#-7";
    case Note::A8: return "A-8";
    case Note::As8: return "A#-8";
    case Note::B8: return "B-8";
    case Note::C8: return "C-8";
    case Note::Cs8: return "C#-8";
    case Note::D8: return "D-8";
    case Note::Ds8: return "D#-8";
    case Note::E8: return "E-8";
    case Note::F8: return "F-8";
    case Note::Fs8: return "F#-8";
    case Note::G8: return "G-8";
    case Note::Gs8: return "G#-8";
    case Note::A9: return "A-9";
    case Note::As9: return "A#-9";
    case Note::B9: return "B-9";
    case Note::C9: return "C-9";
    case Note::Cs9: return "C#-9";
    case Note::D9: return "D-9";
    case Note::Ds9: return "D#-9";
    case Note::E9: return "E-9";
    case Note::F9: return "F-9";
    case Note::Fs9: return "F#-9";
    case Note::G9: return "G-9";
    case Note::Gs9: return "G#-9";
  }
}

} // namespace core
} // namespace qPs3
