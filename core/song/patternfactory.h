#pragma once

#include "core/song/song.h"
#include "util/id.h"

namespace qPs3 {
namespace core {
class Session;

[[nodiscard]] Pattern CreatePattern(Session& session);

} // namespace core
} // namespace qPs3
