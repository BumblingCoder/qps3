#pragma once

#include <memory>
#include <unordered_map>
#include <vector>

#include <boost/hana.hpp>
#include <boost/hana/type.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include "core/machinenetwork/connection.h"
#include "core/machinenetwork/machine.h"

#include "core/song/note.h"

#include "hanaoperators.h"
#include "id.h"
#include "reflectiontag.h"

namespace qPs3 {
namespace core {

struct NoteCell : public utils::Reflectable {
  BOOST_HANA_DEFINE_STRUCT(NoteCell,
                           (Note, note),
                           (utils::ID<Machine>, machine),
                           (uint8_t, volume),
                           (uint8_t, pan),
                           (uint8_t, delay));
  QPSYHANACOMPARE
  constexpr NoteCell() : note(Note::Empty), volume(0), pan(0), delay(0) {}
};

static_assert(std::is_standard_layout<NoteCell>::value, "NoteCell isn't pod");

struct FxCell : public utils::Reflectable {
  BOOST_HANA_DEFINE_STRUCT(FxCell,
                           (utils::ID<Machine>, machine),
                           (uint8_t, delay),
                           (uint8_t, fxNum),
                           (uint8_t, fxVal));
  QPSYHANACOMPARE
  constexpr FxCell() : delay(0), fxNum(0), fxVal(0) {}
};

struct Track : public utils::Reflectable {
  BOOST_HANA_DEFINE_STRUCT(Track,
                           (utils::ID<Track>, id),
                           (QString, name),
                           (size_t, noteColumns),
                           (size_t, fxColumns),
                           (boost::optional<utils::ID<Machine>>, forcedMachine));
  QPSYHANACOMPARE
  Track() : noteColumns(0), fxColumns(0) {}
};

struct NoteColumn : public utils::Reflectable {
  BOOST_HANA_DEFINE_STRUCT(NoteColumn, (std::vector<NoteCell>, notes));
  QPSYHANACOMPARE
};
struct FxColumn : public utils::Reflectable {
  BOOST_HANA_DEFINE_STRUCT(FxColumn, (std::vector<FxCell>, fx));
  QPSYHANACOMPARE
};

struct PatternTrack : public utils::Reflectable {
  BOOST_HANA_DEFINE_STRUCT(PatternTrack,
                           (std::vector<NoteColumn>, noteColumns),
                           (std::vector<FxColumn>, fxColumns));
  QPSYHANACOMPARE
};

struct Pattern : public utils::Reflectable {
  BOOST_HANA_DEFINE_STRUCT(Pattern,
                           (utils::ID<Pattern>, id),
                           (QString, name),
                           (std::vector<PatternTrack>, items));
  QPSYHANACOMPARE
};

class Song : public utils::Reflectable {
public:
  Song();
  BOOST_HANA_DEFINE_STRUCT(
      Song,
      (QString, title),
      (QString, artist),
      (QString, comments),
      //                           (std::vector<std::unique_ptr<Machine>>, machines),
      //                           (std::vector<Connection>, connections),
      (std::vector<Track>, tracks),
      (std::vector<Pattern>, patterns),
      (std::vector<utils::ID<Pattern>>, patternLayout));
  QPSYHANACOMPARE
};
} // namespace core
} // namespace qPs3
