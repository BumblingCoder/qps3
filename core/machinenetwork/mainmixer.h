#pragma once

#include "machine.h"

namespace qPs3 {
namespace core {

class MainMixer : public Machine {
public:
  MainMixer();

  // Machine interface
public:
  QString name() const override;
  MachineType machineType() const override;
};
} // namespace core
} // namespace qPs3
