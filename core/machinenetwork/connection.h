#pragma once

namespace qPs3 {
namespace core {

class Machine;

struct Plug {
  const Machine* machine;
  const int portIndex;
};

struct Connection {
  Plug upstream;
  Plug downstream;
};
} // namespace core
} // namespace qPs3
