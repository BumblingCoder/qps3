#include "machine.h"

#include "core/fileio/jsonio.h"

namespace qPs3 {
namespace core {
Machine::~Machine() = default;

QJsonValue Machine::toJson() const {
  return "machines base";
  //  return io::jsonValue(*this);
}

void Machine::fromJson(const QJsonValue& /*unused*/) {
  //  return io::jsonValue(*this);
}
} // namespace core
} // namespace qPs3
