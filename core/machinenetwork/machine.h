#pragma once

#include <QPoint>
#include <QString>

#include <boost/hana.hpp>
#include <vector>

#include "id.h"
#include "reflectiontag.h"
#include "serializable.h"

namespace qPs3 {
namespace core {

enum class MachineType { Mixer, Generator, Effeect };

struct Port {
  int channels = 2;
};

class Machine : public utils::Serializable, public utils::Reflectable {
public:
  Machine() = default;
  virtual ~Machine();
  Machine(const Machine&) = delete;
  Machine(Machine&&) = delete;
  Machine& operator=(const Machine&) = delete;
  Machine& operator=(const Machine&&) = delete;
  virtual QString name() const = 0;
  virtual MachineType machineType() const = 0;

  QJsonValue toJson() const override;
  void fromJson(const QJsonValue& v) override;

  BOOST_HANA_DEFINE_STRUCT(Machine,
                           (utils::ID<Machine>, id),
                           (std::vector<Port>, inputs),
                           (std::vector<Port>, outputs),
                           (QString, userName),
                           (QPoint, position));
};
} // namespace core
} // namespace qPs3
