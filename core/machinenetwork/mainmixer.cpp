#include "mainmixer.h"
namespace qPs3 {
namespace core {

MainMixer::MainMixer() {
  id = utils::ID<Machine>(0);
  inputs = {{}};
  userName = "Mixer";
}

QString MainMixer::name() const { return "Mixer"; }

MachineType MainMixer::machineType() const { return MachineType::Mixer; }
} // namespace core
} // namespace qPs3
