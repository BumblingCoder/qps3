#include "session.h"

#include <QFile>
#include <QJsonDocument>

#include "core/fileio/jsonio.h"
#include "core/song/patternfactory.h"

#include "util/range.h"

namespace qPs3 {
namespace core {

Session::Session() {
  constexpr const size_t defaultTracks = 10;
  for(int i : utils::range(defaultTracks)) {
    Track t;
    t.name = tr("Track %1").arg(i);
    t.noteColumns = 1;
    t.fxColumns = 1;
    song.tracks.push_back(t);
  }
  song.patterns.emplace_back(CreatePattern(*this));
}

void Session::openFile(const QString& path) {
  QFile file(path);
  file.open(QFile::ReadOnly);
  QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
  core::io::fromJson(song, doc.object());
  emit songLoaded();
}

void Session::saveFile(const QString& path) {
  QFile file(path);
  file.open(QFile::WriteOnly);
  auto json = QJsonDocument(core::io::jsonValue(song));
  file.write(json.toJson());
  emit songSaved();
}

} // namespace core
} // namespace qPs3
