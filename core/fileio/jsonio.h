#pragma once

#include <QDataStream>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <boost/hana.hpp>

#include "core/song/song.h"
#include "serializable.h"

namespace qPs3 {
namespace core {
namespace io {

inline QJsonValue jsonValue(const QString& s) { return s; }
inline QJsonValue jsonValue(int i) { return i; }
inline QJsonValue jsonValue(uint8_t i) { return i; }
inline QJsonValue jsonValue(size_t i) { return QString::number(i); }
inline QJsonValue jsonValue(Note n) { return static_cast<int>(n); }
inline QJsonValue jsonValue(const utils::Serializable* s) { return s->toJson(); }
template <class T> QJsonValue jsonValue(utils::ID<T> id) { return id._id; }
template <class T> QJsonValue jsonValue(boost::optional<T> o) {
  if(!o) { return QJsonValue::Null; }
  return jsonValue(*o);
}

template <class T>
typename std::enable_if<T::reflectable::value, QJsonObject>::type jsonValue(const T& t);
template <typename Xs>
typename std::enable_if<std::is_default_constructible<typename Xs::value_type>::value,
                        QJsonArray>::type
jsonValue(const Xs& xs);

template <class T, class = decltype(std::declval<QDataStream&>() << std::declval<T>())>
QJsonValue jsonValue(const T& t) {
  QByteArray ba;
  QDataStream ds(&ba, QIODevice::WriteOnly);
  ds << t;
  return {ba.toBase64().data()};
}

template <class T>
typename std::enable_if<T::reflectable::value, QJsonObject>::type jsonValue(const T& t) {
  QJsonObject obj;
  boost::hana::for_each(boost::hana::keys(t), [&](const auto& key) {
    const auto& value = boost::hana::at_key(t, key);
    obj[boost::hana::to<const char*>(key)] = jsonValue(value);
  });
  return obj;
}

template <typename Xs>
typename std::enable_if<std::is_default_constructible<typename Xs::value_type>::value,
                        QJsonArray>::type
jsonValue(const Xs& xs) {
  QJsonArray array;
  for(const auto& x : xs) { array.append(jsonValue(x)); }
  return array;
}

template <class T> QJsonValue jsonValue(const std::unique_ptr<T>& t) { return jsonValue(*t); }

// File Read;

// Basic Types;
inline void fromJson(QString& s, const QJsonValue& v) { s = v.toString(); }
inline void fromJson(int& i, const QJsonValue& v) { i = v.toInt(); }
inline void fromJson(uint8_t& i, const QJsonValue& v) { i = static_cast<uint8_t>(v.toInt()); }
inline void fromJson(size_t& i, const QJsonValue& v) { i = v.toString().toULongLong(); }
inline void fromJson(Note& n, const QJsonValue& v) { n = static_cast<Note>(v.toInt()); }
template <class T> inline void fromJson(utils::ID<T>& id, const QJsonValue& v) {
  id._id = v.toInt();
}

// Declare these upfront so they can call eachother.
template <class T> void fromJson(boost::optional<T>& o, QJsonValue v);
template <class T>
typename std::enable_if<T::reflectable::value, void>::type fromJson(T& t, const QJsonValue& v);
template <typename Xs>
typename std::enable_if<std::is_default_constructible<typename Xs::value_type>::value, void>::type
fromJson(Xs& xs, const QJsonValue& v);

template <class T> void fromJson(boost::optional<T>& o, QJsonValue v) {
  if(!v.isNull()) {
    T t;
    fromJson(t, v);
    o = t;
  }
}

template <class T>
typename std::enable_if<T::reflectable::value, void>::type fromJson(T& t, const QJsonValue& v) {
  QJsonObject obj = v.toObject();
  boost::hana::for_each(boost::hana::keys(t), [&](const auto& key) {
    auto& value = boost::hana::at_key(t, key);
    fromJson(value, obj[boost::hana::to<const char*>(key)]);
  });
}

template <typename Xs>
typename std::enable_if<std::is_default_constructible<typename Xs::value_type>::value, void>::type
fromJson(Xs& xs, const QJsonValue& v) {
  QJsonArray array = v.toArray();
  xs.clear();
  for(auto item : array) {
    typename Xs::value_type vt;
    fromJson(vt, item);
    xs.push_back(vt);
  }
}

template <class T, class = decltype(std::declval<QDataStream&>() << std::declval<T>())>
void fromJson(T& t, const QJsonValue& v) {
  QByteArray ba = v.toString().toLocal8Bit();
  QDataStream ds(&ba, QIODevice::ReadOnly);
  ds >> t;
}

template <class T> T fromJson(const QJsonValue& v) {
  T t;
  fromJson(t, v);
  return t;
}
} // namespace io
} // namespace core
} // namespace qPs3
