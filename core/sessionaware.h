#pragma once

namespace qPs3 {
namespace core {

class Session;

class SessionAware {
public:
  void setSession(Session* s);
  virtual void sessionChanged();

protected:
  Session* session = nullptr;
};

} // namespace core
} // namespace qPs3
