#pragma once

#include <QObject>

#include "core/song/song.h"

namespace qPs3 {
namespace gui {
class GuiSession;
}
namespace core {

class Session : public QObject {
  Q_OBJECT
public:
  Session();
  Song song;
  gui::GuiSession* guiSession = nullptr;
  int defaultPatternLines = 64;
  int nextPatternId = 0;

public:
  void openFile(const QString& path);
  void saveFile(const QString& path);

signals:
  void songLoaded();
  void songSaved();
};

} // namespace core
} // namespace qPs3
