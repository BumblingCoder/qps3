#include "sessionaware.h"

namespace qPs3 {
namespace core {

void SessionAware::setSession(Session* s) {
  session = s;
  sessionChanged();
}
void SessionAware::sessionChanged() {}

} // namespace core
} // namespace qPs3
