import qbs

Module {
    Depends {
        name: "cpp"
    }
    cpp.dynamicLibraries: ["gtest"]
}
