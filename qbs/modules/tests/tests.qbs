import qbs

Module {
    Depends {
        name: "cpp"
    }
    Depends {
        name: "GTest"
    }
    Group {
        files: project.sourceDirectory + "/test/unittestmain.cpp"
    }
    additionalProductTypes: ["application", "autotest"]
    cpp.rpaths: project.buildDirectory + "/install-root"
}
