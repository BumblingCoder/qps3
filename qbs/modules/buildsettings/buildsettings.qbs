import qbs

Module {
    Depends {
        name: "cpp"
    }
    Group {
        name: "files"
        files: product.sourceDirectory + "/**"
    }
    cpp.cxxLanguageVersion: "c++1z"
}
