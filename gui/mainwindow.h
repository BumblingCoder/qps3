#pragma once

#include <QMainWindow>

#include "core/session.h"
#include "core/song/song.h"
#include "gui/options/optionsdialog.h"
namespace Ui {
class MainWindow;
}

namespace qPs3 {
namespace gui {

class PatternView;

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = nullptr);
  MainWindow(const MainWindow&) = delete;
  MainWindow(const MainWindow&&) = delete;
  MainWindow& operator=(const MainWindow&) = delete;
  MainWindow& operator=(const MainWindow&&) = delete;
  ~MainWindow() override;

private:
  void openFile();
  void saveFile();

private:
  Ui::MainWindow* ui = nullptr;
  OptionsDialog optionsDialog;
  core::Session session;
};
} // namespace gui
} // namespace qPs3
