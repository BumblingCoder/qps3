import qbs

Library {
    name: "gui"
    Depends {
        name: "buildsettings"
    }
    Depends {
        name: "core"
    }
    Depends {
        name: "util"
    }
    Depends {
        name: "Qt"
        submodules: ["core", "gui", "widgets"]
    }
    Group {
        fileTagsFilter: product.type
        qbs.install: true
    }
    Export {
        Depends {
            name: "Qt"
            submodules: ["core", "gui", "widgets"]
        }
    }
}
