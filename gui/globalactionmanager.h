#pragma once

#include <QAction>
#include <QSettings>

namespace qPs3 {
namespace gui {

class GlobalActionManager {
public:
  GlobalActionManager(QSettings& settings);
  void addAction(QAction* action, const QString& context);

  void setKeybinding(const QString& context, const QString& name, const QKeySequence& sequence);

  struct ActionGroup {
    QString context;
    std::vector<std::pair<QString, QAction*>> actions;
  };
  const std::vector<ActionGroup>& availableActions() const;

private:
  void addActionToActionsList(QAction* action, const QString& context);

private:
  QSettings& m_settings;
  std::vector<ActionGroup> actions;
};

} // namespace gui
} // namespace qPs3
