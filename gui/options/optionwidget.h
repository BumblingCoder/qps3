#pragma once

#include <QString>
#include <QWidget>

namespace qPs3 {
namespace gui {

class OptionWidget : public QWidget {
public:
  OptionWidget(QWidget* parent) : QWidget(parent) {}
  virtual QString name() const = 0;
  virtual QString icon() const = 0;
};

} // namespace gui
} // namespace qPs3
