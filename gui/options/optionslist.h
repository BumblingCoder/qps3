#pragma once

#include <QAbstractListModel>

#include "optionwidget.h"

namespace qPs3 {
namespace gui {

class OptionsList : public QAbstractListModel {
  Q_OBJECT

public:
  explicit OptionsList(std::vector<OptionWidget*>& options, QObject* parent = nullptr);

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

private:
  std::vector<OptionWidget*>& optionWidgets;
};

} // namespace gui
} // namespace qPs3
