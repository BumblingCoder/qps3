#pragma once

#include <QDialog>

#include "optionwidget.h"

namespace Ui {
class OptionsDialog;
}

namespace qPs3 {
namespace gui {

class OptionsDialog : public QDialog {
  Q_OBJECT

public:
  explicit OptionsDialog(QWidget* parent);
  ~OptionsDialog() override;
  OptionsDialog(OptionsDialog&) = delete;
  OptionsDialog(OptionsDialog&&) = delete;
  OptionsDialog operator=(OptionsDialog&) = delete;
  OptionsDialog operator=(OptionsDialog&&) = delete;

private:
  Ui::OptionsDialog* ui;
  std::vector<OptionWidget*> optionsWidgets;
};

} // namespace gui
} // namespace qPs3
