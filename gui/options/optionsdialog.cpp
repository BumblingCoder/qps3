#include "optionsdialog.h"
#include "ui_optionsdialog.h"

#include "keybindingswidget.h"
#include "optionslist.h"

namespace qPs3 {
namespace gui {

OptionsDialog::OptionsDialog(QWidget* parent) : QDialog(parent), ui(new Ui::OptionsDialog) {
  ui->setupUi(this);
  optionsWidgets.push_back(new KeyBindingsWidet(this));
  auto options = new OptionsList(optionsWidgets, this);
  ui->pageList->setModel(options);

  for(auto widget : optionsWidgets) { ui->settingsList->addWidget(widget); }
}

OptionsDialog::~OptionsDialog() { delete ui; }

} // namespace gui
} // namespace qPs3
