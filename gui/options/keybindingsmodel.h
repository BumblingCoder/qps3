#pragma once

#include <QAbstractItemModel>

#include "core/sessionaware.h"

namespace qPs3 {
namespace gui {

class KeyBindingsModel : public QAbstractItemModel, public core::SessionAware {
  Q_OBJECT

public:
  explicit KeyBindingsModel(QObject* parent = nullptr);

  // Basic functionality:
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  // Editable:
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  QModelIndex index(int row, int column, const QModelIndex& parent) const override;
  QModelIndex parent(const QModelIndex& child) const override;
};

} // namespace gui
} // namespace qPs3
