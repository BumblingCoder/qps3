#include "keybindingswidget.h"
#include "keybindingsmodel.h"

#include <QObject>
#include <QTreeView>
#include <QVBoxLayout>

namespace qPs3 {
namespace gui {

KeyBindingsWidet::KeyBindingsWidet(QWidget* parent) : OptionWidget(parent) {
  auto layout = new QHBoxLayout(this);
  layout->setMargin(0);
  auto view = new QTreeView(this);
  view->setModel(new KeyBindingsModel(this));
  layout->addWidget(view);
}

QString KeyBindingsWidet::name() const { return QObject::tr("Keybindings"); }

QString KeyBindingsWidet::icon() const { return "input-keyboard"; }

} // namespace gui
} // namespace qPs3
