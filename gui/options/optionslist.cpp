#include "optionslist.h"

#include <QIcon>

namespace qPs3 {
namespace gui {

OptionsList::OptionsList(std::vector<OptionWidget*>& options, QObject* parent)
    : QAbstractListModel(parent), optionWidgets(options) {}

int OptionsList::rowCount(const QModelIndex& parent) const {
  if(parent.isValid()) { return 0; }
  return optionWidgets.size();
}

QVariant OptionsList::data(const QModelIndex& index, int role) const {
  if(role == Qt::DisplayRole) { return optionWidgets[index.row()]->name(); }
  if(role == Qt::DecorationRole) { return QIcon::fromTheme(optionWidgets[index.row()]->icon()); }
  return {};
}

} // namespace gui
} // namespace qPs3
