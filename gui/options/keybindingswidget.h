#pragma once

#include "optionwidget.h"

namespace qPs3 {
namespace gui {

class KeyBindingsWidet : public OptionWidget {
public:
  KeyBindingsWidet(QWidget* parent);
  QString name() const override;
  QString icon() const override;
};

} // namespace gui
} // namespace qPs3
