#include "keybindingsmodel.h"

#include "core/session.h"
#include "gui/globalactionmanager.h"
#include "gui/guisession.h"

namespace qPs3 {
namespace gui {

KeyBindingsModel::KeyBindingsModel(QObject* parent) : QAbstractItemModel(parent) {}

int KeyBindingsModel::rowCount(const QModelIndex& parent) const {
  auto actions = session->guiSession->actionManager.availableActions();
  if(!parent.isValid()) { return actions.size(); }
  if(parent.parent().isValid()) { return 0; }
  return actions[parent.row()].actions.size();
}

int KeyBindingsModel::columnCount(const QModelIndex& /*parent*/) const { return 2; }

QVariant KeyBindingsModel::data(const QModelIndex& index, int role) const {
  if(!index.isValid()) { return QVariant(); }
  if(role != Qt::DisplayRole) { return QVariant(); }

  const auto& actions = session->guiSession->actionManager.availableActions();
  if(!index.parent().isValid()) {
    if(index.column() == 0) { return actions[index.row()].context; }
    return {};
  }

  if(index.column() == 0) { return actions[index.parent().row()].actions[index.row()].first; }
  return actions[index.parent().row()].actions[index.row()].second->shortcut().toString(
      QKeySequence::SequenceFormat::NativeText);
}

bool KeyBindingsModel::setData(const QModelIndex& index, const QVariant& value, int role) {
  if(data(index, role) != value) {
    // FIXME: Implement me!
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
  }
  return false;
}

Qt::ItemFlags KeyBindingsModel::flags(const QModelIndex& index) const {
  if(!index.isValid()) { return Qt::NoItemFlags; }

  return Qt::ItemIsEditable; // FIXME: Implement me!
}

QModelIndex KeyBindingsModel::index(int row, int column, const QModelIndex& parent) const {
  if(parent.parent().isValid()) { return {}; };
  if(parent.isValid()) { return createIndex(row, column, parent.row()); }
  return createIndex(row, column, -1);
}

QModelIndex KeyBindingsModel::parent(const QModelIndex& child) const {
  if(child.internalId() == -1ul) { return {}; };
  return index(child.internalId(), 0, {});
}

} // namespace gui
} // namespace qPs3
