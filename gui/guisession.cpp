#include "guisession.h"

#include "core/session.h"
namespace qPs3 {
namespace gui {
utils::ID<core::Pattern> GuiSession::currentPattern() const { return m_currentPattern; }

void GuiSession::setCurrentPattern(utils::ID<core::Pattern> currentPattern) {
  if(m_currentPattern == currentPattern) { return; }
  m_currentPattern = currentPattern;
  emit currentPatternChanged(m_currentPattern);
}
} // namespace gui
} // namespace qPs3
