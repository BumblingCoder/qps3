#include "globalactionmanager.h"

namespace qPs3 {
namespace gui {

GlobalActionManager::GlobalActionManager(QSettings& settings) : m_settings(settings) {}

void GlobalActionManager::addAction(QAction* action, const QString& context) {
  auto settingName = "keys/" + context + '/' + action->text();
  if(m_settings.contains(settingName)) {
    action->setShortcut(m_settings.value(settingName).toString());
  } else {
    m_settings.setValue(settingName, action->shortcut().toString());
  }
  addActionToActionsList(action, context);
}

void GlobalActionManager::setKeybinding(const QString& context,
                                        const QString& name,
                                        const QKeySequence& sequence) {
  for(auto& group : actions) {
    if(group.context == context) {
      for(auto& action : group.actions) {
        if(action.first == name) {
          action.second->setShortcut(sequence);
          m_settings.setValue("keys/" + context + '/' + name, sequence.toString());
          return;
        }
      }
    }
  }
}

const std::vector<GlobalActionManager::ActionGroup>& GlobalActionManager::availableActions() const {
  return actions;
}

void GlobalActionManager::addActionToActionsList(QAction* action, const QString& context) {
  for(auto& group : actions) {
    if(group.context == context) {
      group.actions.emplace_back(action->text(), action);
      return;
    }
  }
  actions.push_back({context, {{action->text(), action}}});
}

} // namespace gui
} // namespace qPs3
