#pragma once

#include <unordered_map>

#include "core/song/note.h"

namespace qPs3 {
namespace gui {

class KeyMap {
public:
  KeyMap();
  bool isNoteKey(int key) const;
  core::Note noteForKey(int key) const;
  bool isHexKey(int key) const;
  int hexForKey(int key) const;
  bool isIntKey(int key) const;
  int intForKey(int key) const;

  void loadTypingKeyMap(const QString& mapFile);
  void saveTypingKeyMap(const QString& mapFile);

private:
  std::unordered_map<int, core::Note> typingNoteMap;
};

} // namespace gui
} // namespace qPs3
