#pragma once

#include <QObject>
#include <QSettings>
#include <QUndoStack>

#include "core/song/song.h"

#include "util/id.h"

#include "gui/globalactionmanager.h"
#include "gui/keymap.h"
namespace qPs3 {
namespace gui {

class GuiSession : public QObject {
  Q_OBJECT

public:
  Q_PROPERTY(utils::ID<core::Pattern> m_currentPattern READ currentPattern WRITE setCurrentPattern
                 NOTIFY currentPatternChanged)
  GuiSession() = default;

  utils::ID<core::Pattern> currentPattern() const;
signals:
  void currentPatternChanged(utils::ID<core::Pattern> currentPattern);

public slots:
  void setCurrentPattern(utils::ID<core::Pattern> currentPattern);

public:
  QUndoStack undoStack;
  KeyMap keyMap;
  GlobalActionManager actionManager{m_userSettings};

private:
  utils::ID<core::Pattern> m_currentPattern{-1};
  QSettings m_userSettings;
};

} // namespace gui
} // namespace qPs3
