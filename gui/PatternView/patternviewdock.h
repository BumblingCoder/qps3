#include <QDockWidget>

#include "core/sessionaware.h"

namespace Ui {
class PatternViewDock;
}

namespace qPs3 {
namespace gui {

class PatternViewDock : public QDockWidget, public core::SessionAware {
  Q_OBJECT

public:
  explicit PatternViewDock(QWidget* parent = nullptr);
  PatternViewDock(PatternViewDock&) = delete;
  PatternViewDock(PatternViewDock&&) = delete;
  PatternViewDock& operator=(PatternViewDock&) = delete;
  PatternViewDock& operator=(PatternViewDock&&) = delete;
  ~PatternViewDock() override;

private:
  Ui::PatternViewDock* ui{};
};

} // namespace gui
} // namespace qPs3
