#include "patternviewdelegate.h"

#include <QPainter>
#include <QStylePainter>

#include "patternmodel.h"

#include "core/song/note.h"

namespace qPs3 {
namespace gui {

void PatternViewDelegate::paint(QPainter* painter,
                                const QStyleOptionViewItem& option,
                                const QModelIndex& index) const {
  { // Draw default items.
    QStyleOptionViewItem o2 = option;
    switch(index.data(PatternModel::DataTypeRole).toInt()) {
      case PatternModel::NoteData:
        o2.text = core::toString(static_cast<core::Note>(index.data().toInt()));
        break;
      case PatternModel::HexData:
        if(const int number = index.data().toInt();
           number == 0 && index.data(PatternModel::OtherCellRole).toInt() == 0) {
          o2.text = ".";
        } else {
          o2.text = QString::number(index.data().toInt(), 16);
        }
        break;
      case PatternModel::IntData:
        if(const int number = index.data().toInt();
           number == 0 && index.data(PatternModel::OtherCellRole).toInt() == 0) {
          o2.text = ".";
        } else {
          o2.text = QString::number(index.data().toInt(), 10);
        }
        break;
    }
    o2.direction = Qt::RightToLeft;
    QStyledItemDelegate::paint(painter, o2, QModelIndex());
  }

  // Draw the lines.
  if(index.data(PatternModel::BeginningOfTrackRole).toBool()) {
    painter->save();
    painter->setLayoutDirection(Qt::LayoutDirection::RightToLeft);
    auto lineBrush = QBrush(option.palette.text());
    painter->setPen(lineBrush.color());
    painter->fillRect(QRect(option.rect.topLeft(), QSize(2, option.rect.height())), lineBrush);
    painter->drawRect(QRect(option.rect.topLeft(), QSize(2, option.rect.height())));
    painter->restore();
  }
}

QSize PatternViewDelegate::sizeHint(const QStyleOptionViewItem& option,
                                    const QModelIndex& index) const {
  if(index.data(PatternModel::DataTypeRole).toInt() != PatternModel::NoteData) {
    return option.fontMetrics.size(Qt::TextSingleLine, "M");
  }
  return QStyledItemDelegate::sizeHint(option, index);
}

} // namespace gui
} // namespace qPs3
