#pragma once

#include <QAbstractTableModel>

#include <boost/variant.hpp>

#include "core/session.h"
#include "core/sessionaware.h"

#include "gui/guisession.h"
namespace qPs3 {
namespace core {
class Song;
} // namespace core

namespace gui {

class PatternModel : public QAbstractTableModel, public core::SessionAware {
  Q_OBJECT
  enum class NoteColumns {
    note,
    machine_1,
    machine_2,
    volume_1,
    volume_2,
    pan_1,
    pan_2,
    delay_1,
    delay_2,
  };
  enum class FxColumns {
    machine_1,
    machine_2,
    delay_1,
    delay_2,
    fxNum_1,
    fxNum_2,
    fxVal_1,
    fxVal_2,
  };

public:
  enum CustomRoles { BeginningOfTrackRole = Qt::UserRole, DataTypeRole, OtherCellRole };
  enum DataTypes { NoteData, IntData, HexData };
  explicit PatternModel(QObject* parent = nullptr);
  void setCurrentPattern();

  // Header:
  QVariant
  headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  // Basic functionality:
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  // Editable:
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  void sessionChanged() override;

private:
  struct DataStructurePosition {
    struct NotePosition {
      const size_t column;
      const size_t cell;
    };
    struct FxPosition {
      const size_t column;
      const size_t cell;
    };
    const size_t track;
    const boost::variant<NotePosition, FxPosition> position;
  };

  DataStructurePosition findTrack(size_t column) const;

  DataTypes typeForColumn(const DataStructurePosition& /*p*/) const;

  core::NoteCell& cell(int track, int row, const DataStructurePosition::NotePosition& p) const;
  core::FxCell& cell(int track, int row, const DataStructurePosition::FxPosition& p) const;
  QModelIndex otherIndex(const QModelIndex& thisIndex, const DataStructurePosition& p) const;

private:
  std::optional<size_t> pattern;
};
} // namespace gui
} // namespace qPs3
