#include "patternmodel.h"

#include <exception>

#include "core/session.h"
#include "core/song/song.h"

#include "gui/guisession.h"
#include "gui/undocommands/genericdataundocommand.h"

namespace qPs3 {
namespace gui {

static constexpr int NumberOfCells(uint8_t /*unused*/) { return 2; }
static constexpr int NumberOfCells(core::Note /*unused*/) { return 1; }
static constexpr int NumberOfCells(utils::ID<core::Machine> /*unused*/) { return 2; }
template <class T> constexpr int NumberOfCells(const T& t) {
  int cells = 0;
  boost::hana::for_each(boost::hana::keys(t),
                        [&](auto k) { cells += NumberOfCells(boost::hana::at_key(t, k)); });
  return cells;
}

static constexpr const size_t numFxColumns = NumberOfCells(core::FxCell{});
static constexpr const size_t numNoteColumns = NumberOfCells(core::NoteCell{});

PatternModel::PatternModel(QObject* parent) : QAbstractTableModel(parent) {}

QVariant PatternModel::headerData(int section, Qt::Orientation orientation, int role) const {
  if(role != Qt::DisplayRole) { return {}; }
  if(orientation == Qt::Vertical) { return section; }
  auto track = findTrack(static_cast<size_t>(section));
  auto noteColumn = boost::get<DataStructurePosition::NotePosition>(&track.position);
  if((noteColumn != nullptr) && noteColumn->cell == 0 && noteColumn->column == 0) {
    return session->song.tracks[track.track].name;
  }
  return {};
}

int PatternModel::rowCount(const QModelIndex& parent) const {
  if(parent.isValid() || !pattern) { return 0; }
  if(session == nullptr) { return 0; }

  return static_cast<int>(session->song.patterns[*pattern].items[0].noteColumns[0].notes.size());
}

int PatternModel::columnCount(const QModelIndex& parent) const {
  if(parent.isValid() || !pattern) { return 0; }
  if(session == nullptr) { return 0; }

  int columns = 0;
  for(auto track : session->song.tracks) {
    columns += track.noteColumns * numNoteColumns + track.fxColumns * numFxColumns;
  }
  return columns;
}

QVariant PatternModel::data(const QModelIndex& index, int role) const {
  if(!index.isValid() || !pattern) { return QVariant(); }

  auto position = findTrack(static_cast<size_t>(index.column()));
  if(role == BeginningOfTrackRole) {
    auto noteColumn = boost::get<DataStructurePosition::NotePosition>(&position.position);
    if((noteColumn != nullptr) && noteColumn->cell == 0 && noteColumn->column == 0) { return true; }
    return false;
  }

  if(role == DataTypeRole) { return typeForColumn(position); }

  if(role == Qt::ItemDataRole::TextAlignmentRole) { return Qt::AlignRight; }

  if(role == OtherCellRole) { return data(otherIndex(index, position)); }
  if(role != Qt::DisplayRole) { return {}; }

  // display role
  if(auto note = boost::get<DataStructurePosition::NotePosition>(&position.position)) {
    auto cell = this->cell(position.track, index.row(), *note);
    switch(static_cast<NoteColumns>(note->cell)) {
      case NoteColumns::note: return static_cast<int>(cell.note);
      case NoteColumns::machine_1: return (cell.machine._id / 10);
      case NoteColumns::machine_2: return cell.machine._id % 10;
      case NoteColumns::volume_1: return (cell.volume & 0xf0) >> 4;
      case NoteColumns::volume_2: return cell.volume & 0x0f;
      case NoteColumns::pan_1: return (cell.pan & 0xf0) >> 4;
      case NoteColumns::pan_2: return cell.pan & 0x0f;
      case NoteColumns::delay_1: return (cell.delay & 0xf0) >> 4;
      case NoteColumns::delay_2: return cell.delay & 0x0f;
    }
  } else {
    auto fx = boost::get<DataStructurePosition::FxPosition>(position.position);
    auto cell = this->cell(position.track, index.row(), fx);
    switch(static_cast<FxColumns>(fx.cell)) {
      case FxColumns::machine_1: return (cell.machine._id / 10);
      case FxColumns::machine_2: return cell.machine._id % 10;
      case FxColumns::delay_1: return (cell.delay & 0xf0) >> 4;
      case FxColumns::delay_2: return cell.delay & 0x0f;
      case FxColumns::fxNum_1: return (cell.fxNum & 0xf0) >> 4;
      case FxColumns::fxNum_2: return cell.fxNum & 0x0f;
      case FxColumns::fxVal_1: return (cell.fxVal & 0xf0) >> 4;
      case FxColumns::fxVal_2: return cell.fxVal & 0x0f;
    }
  }
  return {};
}

bool PatternModel::setData(const QModelIndex& index, const QVariant& value, int role) {
  if(data(index, role) == value) { return false; }

  auto position = findTrack(static_cast<size_t>(index.column()));
  using namespace core;
  auto& undoStack = session->guiSession->undoStack;
  if(auto note = boost::get<DataStructurePosition::NotePosition>(&position.position)) {
    auto cell = [&]() -> NoteCell& { return this->cell(position.track, index.row(), *note); };
    switch(static_cast<NoteColumns>(note->cell)) {
      case NoteColumns::note:
        undoStack.push(new undo::GenericUndoCommand(
            cell, &NoteCell::note, static_cast<Note>(value.toInt()), this, index));
        break;
      case NoteColumns::machine_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::machine,
            utils::ID<Machine>((value.toInt() * 10) + cell().machine._id % 10),
            this,
            index));
        break;
      case NoteColumns::machine_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::machine,
            utils::ID<Machine>((value.toInt()) + (cell().machine._id / 10) * 10),
            this,
            index));
        break;
      case NoteColumns::volume_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::volume,
            static_cast<uint8_t>((value.toInt() << 4) + (cell().volume & 0xf)),
            this,
            index));
        break;
      case NoteColumns::volume_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::volume,
            static_cast<uint8_t>((value.toInt()) + (cell().volume & 0xf0)),
            this,
            index));
        break;
      case NoteColumns::pan_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::pan,
            static_cast<uint8_t>((value.toInt() << 4) + (cell().pan & 0xf)),
            this,
            index));
        break;
      case NoteColumns::pan_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::pan,
            static_cast<uint8_t>((value.toInt()) + (cell().pan & 0xf0)),
            this,
            index));
        break;
      case NoteColumns::delay_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::delay,
            static_cast<uint8_t>((value.toInt() << 4) + (cell().delay & 0xf)),
            this,
            index));
        break;
      case NoteColumns::delay_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &NoteCell::delay,
            static_cast<uint8_t>((value.toInt()) + (cell().delay & 0xf0)),
            this,
            index));
        break;
    }
  } else {
    auto fx = boost::get<DataStructurePosition::FxPosition>(position.position);
    auto cell = [&]() -> FxCell& { return this->cell(position.track, index.row(), fx); };
    switch(static_cast<FxColumns>(fx.cell)) {
      case FxColumns::machine_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::machine,
            utils::ID<Machine>((value.toInt() * 10) + cell().machine._id % 10),
            this,
            index));
        break;
        ;
      case FxColumns::machine_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::machine,
            utils::ID<Machine>((value.toInt()) + (cell().machine._id / 10) * 10),
            this,
            index));
        break;

      case FxColumns::delay_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::delay,
            static_cast<uint8_t>((value.toInt() << 4) + (cell().delay & 0xf)),
            this,
            index));
        break;

      case FxColumns::delay_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::delay,
            static_cast<uint8_t>((value.toInt()) + (cell().delay & 0xf0)),
            this,
            index));
        break;
      case FxColumns::fxNum_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::fxNum,
            static_cast<uint8_t>((value.toInt() << 4) + (cell().fxNum & 0xf)),
            this,
            index));
        break;
      case FxColumns::fxNum_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::fxNum,
            static_cast<uint8_t>((value.toInt()) + (cell().fxNum & 0xf0)),
            this,
            index));
        break;
      case FxColumns::fxVal_1:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::fxVal,
            static_cast<uint8_t>((value.toInt() << 4) + (cell().fxVal & 0xf)),
            this,
            index));
        break;
      case FxColumns::fxVal_2:
        undoStack.push(new undo::GenericUndoCommand(
            cell,
            &FxCell::fxVal,
            static_cast<uint8_t>((value.toInt()) + (cell().fxVal & 0xf0)),
            this,
            index));
        break;
    }
  }
  emit dataChanged(index, otherIndex(index, position), QVector<int>() << role);
  return true;
}

Qt::ItemFlags PatternModel::flags(const QModelIndex& index) const {
  if(!index.isValid()) { return Qt::NoItemFlags; }

  return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

void PatternModel::sessionChanged() {
  emit beginResetModel();
  connect(session->guiSession, &GuiSession::currentPatternChanged, this, [&](auto pattern) {
    beginResetModel();
    for(size_t i = 0; i < session->song.patterns.size(); ++i) {
      if(pattern == session->song.patterns[i].id) { this->pattern = i; }
    }
    endResetModel();
  });
  connect(session, &core::Session::songLoaded, this, [&]() {
    beginResetModel();
    endResetModel();
  });
  emit endResetModel();
}

//!
//! \brief PatternModel::findTrack
//! \param column Column index to search for
//! \return pattern track, column number, item type
//!
PatternModel::DataStructurePosition PatternModel::findTrack(size_t column) const {
  size_t consumedColumns = 0;
  for(size_t track = 0; track < session->song.tracks.size(); ++track) {
    for(size_t noteTrack = 0; noteTrack < session->song.tracks[track].noteColumns; ++noteTrack) {

      if((column - consumedColumns) < numNoteColumns) {
        return {track, DataStructurePosition::NotePosition{noteTrack, column - consumedColumns}};
      }
      consumedColumns += numNoteColumns;
    }
    for(size_t fxTrack = 0; fxTrack < session->song.tracks[track].fxColumns; ++fxTrack) {
      if((column - consumedColumns) < numFxColumns) {
        return {track, DataStructurePosition::FxPosition{fxTrack, column - consumedColumns}};
      }
      consumedColumns += numFxColumns;
    }
  }
  throw std::runtime_error("Couldn't find position for column " + std::to_string(column));
}

PatternModel::DataTypes PatternModel::typeForColumn(const DataStructurePosition& p) const {
  if(auto note = boost::get<DataStructurePosition::NotePosition>(&p.position)) {
    switch(static_cast<NoteColumns>(note->cell)) {
      case NoteColumns::note: return NoteData;
      case NoteColumns::machine_1: return IntData;
      case NoteColumns::machine_2: return IntData;
      case NoteColumns::volume_1: return HexData;
      case NoteColumns::volume_2: return HexData;
      case NoteColumns::pan_1: return HexData;
      case NoteColumns::pan_2: return HexData;
      case NoteColumns::delay_1: return HexData;
      case NoteColumns::delay_2: return HexData;
    }
  } else {
    auto fx = boost::get<DataStructurePosition::FxPosition>(&p.position);
    switch(static_cast<FxColumns>(fx->cell)) {
      case FxColumns::machine_1: return IntData;
      case FxColumns::machine_2: return IntData;
      case FxColumns::delay_1: return HexData;
      case FxColumns::delay_2: return HexData;
      case FxColumns::fxNum_1: return HexData;
      case FxColumns::fxNum_2: return HexData;
      case FxColumns::fxVal_1: return HexData;
      case FxColumns::fxVal_2: return HexData;
    }
    throw std::runtime_error("Couldn't find type for cell");
  }
}

core::NoteCell&
PatternModel::cell(int track, int row, const DataStructurePosition::NotePosition& p) const {
  return session->song.patterns[*pattern].items[track].noteColumns[p.column].notes[row];
}

core::FxCell&
PatternModel::cell(int track, int row, const DataStructurePosition::FxPosition& p) const {
  return session->song.patterns[*pattern].items[track].fxColumns[p.column].fx[row];
}

QModelIndex PatternModel::otherIndex(const QModelIndex& thisIndex,
                                     const PatternModel::DataStructurePosition& p) const {
  if(auto note = boost::get<DataStructurePosition::NotePosition>(&p.position)) {
    switch(static_cast<NoteColumns>(note->cell)) {
      case NoteColumns::note: return thisIndex;
      case NoteColumns::machine_1:
      case NoteColumns::volume_1:
      case NoteColumns::pan_1:
      case NoteColumns::delay_1: return this->index(thisIndex.row(), thisIndex.column() + 1);
      case NoteColumns::machine_2:
      case NoteColumns::volume_2:
      case NoteColumns::pan_2:
      case NoteColumns::delay_2: return this->index(thisIndex.row(), thisIndex.column() - 1);
    }
  } else {
    auto fx = boost::get<DataStructurePosition::FxPosition>(p.position);
    switch(static_cast<FxColumns>(fx.cell)) {
      case FxColumns::machine_1:
      case FxColumns::delay_1:
      case FxColumns::fxNum_1:
      case FxColumns::fxVal_1: return this->index(thisIndex.row(), thisIndex.column() + 1);
      case FxColumns::machine_2:
      case FxColumns::delay_2:
      case FxColumns::fxNum_2:
      case FxColumns::fxVal_2: return this->index(thisIndex.row(), thisIndex.column() - 1);
    }
  }
}

} // namespace gui
} // namespace qPs3
