#include "patterntable.h"

#include <QKeyEvent>

#include "core/session.h"

#include "gui/PatternView/patternmodel.h"
#include "gui/guisession.h"

namespace qPs3 {
namespace gui {
PatternTable::PatternTable(QWidget* parent) : QTableView(parent) {
  setEditTriggers(QAbstractItemView::NoEditTriggers);
  setSelectionMode(QAbstractItemView::ExtendedSelection);
}

void PatternTable::keyPressEvent(QKeyEvent* event) {
  auto currentIndex = selectionModel()->currentIndex();
  if(currentIndex.data(PatternModel::DataTypeRole) == PatternModel::NoteData &&
     session->guiSession->keyMap.isNoteKey(event->key())) {
    model()->setData(currentIndex,
                     static_cast<int>(session->guiSession->keyMap.noteForKey(event->key())));
    event->accept();
    return;
  }
  if(currentIndex.data(PatternModel::DataTypeRole) == PatternModel::IntData &&
     session->guiSession->keyMap.isIntKey(event->key())) {
    model()->setData(currentIndex,
                     static_cast<int>(session->guiSession->keyMap.intForKey(event->key())));
    event->accept();
    return;
  }
  if(currentIndex.data(PatternModel::DataTypeRole) == PatternModel::HexData &&
     session->guiSession->keyMap.isHexKey(event->key())) {
    model()->setData(currentIndex,
                     static_cast<int>(session->guiSession->keyMap.hexForKey(event->key())));
    event->accept();
    return;
  }
  if(event->key() == Qt::Key_Delete) {
    model()->setData(currentIndex, 0);
    event->accept();
    return;
  }
  QTableView::keyPressEvent(event);
}
} // namespace gui
} // namespace qPs3
