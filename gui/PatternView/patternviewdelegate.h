#pragma once

#include <QStyledItemDelegate>

namespace qPs3 {
namespace gui {

class PatternViewDelegate : public QStyledItemDelegate {
public:
  PatternViewDelegate(QObject* parent) : QStyledItemDelegate(parent) {}
  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;
  QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};
} // namespace gui
} // namespace qPs3
