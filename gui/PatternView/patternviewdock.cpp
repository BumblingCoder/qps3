#include "patternviewdock.h"
#include "ui_patternviewdock.h"

#include <QTreeView>

#include "gui/PatternView/patternmodel.h"
#include "gui/PatternView/patternviewdelegate.h"

namespace qPs3 {
namespace gui {

PatternViewDock::PatternViewDock(QWidget* parent)
    : QDockWidget(parent), ui(new Ui::PatternViewDock) {
  ui->setupUi(this);
  ui->tableView->setModel(new PatternModel(this));
  ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
  ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
  connect(ui->tableView->model(), &QAbstractItemModel::modelReset, [&]() {
    ui->tableView->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tableView->verticalHeader()->resizeSections(QHeaderView::ResizeToContents);
  });
  ui->tableView->horizontalHeader()->setMinimumSectionSize(1);
  ui->tableView->setItemDelegate(new PatternViewDelegate(this));
}

PatternViewDock::~PatternViewDock() { delete ui; }

} // namespace gui
} // namespace qPs3
