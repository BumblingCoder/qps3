#pragma once

#include <QTableView>

#include "core/sessionaware.h"

namespace qPs3 {
namespace gui {
class PatternTable : public QTableView, public core::SessionAware {
public:
  PatternTable(QWidget* parent);
  void keyPressEvent(QKeyEvent* event) override;
};

} // namespace gui

} // namespace qPs3
