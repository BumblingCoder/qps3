#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAction>
#include <QFileDialog>

#include "gui/PatternList/patternlistdock.h"
#include "gui/PatternView/patternviewdock.h"
#include "gui/guisession.h"
#include "gui/options/optionsdialog.h"

namespace qPs3 {
namespace gui {

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), optionsDialog(this) {
  ui->setupUi(this);
  setWindowTitle(tr("qPs3"));

  session.guiSession = new GuiSession;

  auto fileMenu = new QMenu(tr("File"), this);
  { // Open
    auto openAction = new QAction(tr("Open"), this);
    openAction->setShortcuts(QKeySequence::keyBindings(QKeySequence::Open));
    session.guiSession->actionManager.addAction(openAction, tr("Application"));
    fileMenu->addAction(openAction);
    connect(openAction, &QAction::triggered, this, &MainWindow::openFile);
  }
  { // Save
    auto saveAction = new QAction(tr("Save"), this);
    saveAction->setShortcuts(QKeySequence::keyBindings(QKeySequence::Save));
    session.guiSession->actionManager.addAction(saveAction, tr("Application"));
    connect(saveAction, &QAction::triggered, this, &MainWindow::saveFile);
    fileMenu->addAction(saveAction);
  }
  auto editMenu = new QMenu(tr("Edit"));
  this->menuBar()->addMenu(fileMenu);
  this->menuBar()->addMenu(editMenu);
  this->centralWidget()->hide();
  this->setDockNestingEnabled(true);
  this->addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, new PatternListDock(this));
  this->addDockWidget(Qt::DockWidgetArea::RightDockWidgetArea, new PatternViewDock(this));

  { // undo
    auto undoAction = session.guiSession->undoStack.createUndoAction(this);
    undoAction->setShortcuts(QKeySequence::keyBindings(QKeySequence::Undo));
    session.guiSession->actionManager.addAction(undoAction, tr("Application"));
    editMenu->addAction(undoAction);
  }
  { // redo
    auto redoAction = session.guiSession->undoStack.createRedoAction(this);
    redoAction->setShortcuts(QKeySequence::keyBindings(QKeySequence::Redo));
    session.guiSession->actionManager.addAction(redoAction, tr("Application"));
    editMenu->addAction(redoAction);
  }
  { // options
    auto optionsAction = new QAction(tr("Options"), this);
    optionsAction->setShortcuts(QKeySequence::keyBindings(QKeySequence::Preferences));
    session.guiSession->actionManager.addAction(optionsAction, tr("Application"));
    editMenu->addAction(optionsAction);
    connect(optionsAction, &QAction::triggered, this, [&]() { optionsDialog.exec(); });
  }

  for(auto widget : findChildren<QObject*>()) {
    auto sessionAware = dynamic_cast<core::SessionAware*>(widget);
    if(sessionAware != nullptr) { sessionAware->setSession(&session); }
  }
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::openFile() {
  QString fileName =
      QFileDialog::getOpenFileName(this, tr("Open"), QString(), tr("QPsy3 File (*.qpsy3)"));
  session.openFile(fileName);
}

void MainWindow::saveFile() {
  QString fileName =
      QFileDialog::getSaveFileName(this, tr("Save"), QString(), tr("QPsy3 File (*.qpsy3)"));
  session.saveFile(fileName);
}
} // namespace gui
} // namespace qPs3
