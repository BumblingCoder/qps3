#include "patternlistdock.h"
#include "ui_patternlistdock.h"

namespace qPs3 {
namespace gui {

PatternListDock::PatternListDock(QWidget* parent)
    : QDockWidget(parent), ui(new Ui::PatternListDock) {
  ui->setupUi(this);
}

PatternListDock::~PatternListDock() { delete ui; }

} // namespace gui
} // namespace qPs3
