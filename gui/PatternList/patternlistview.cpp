#include "patternlistview.h"
#include "ui_patternlistview.h"

#include <QItemSelectionModel>

#include "gui/PatternList/patternlistmodel.h"
#include "gui/guisession.h"

#include "core/session.h"

namespace qPs3 {
namespace gui {

PatternListView::PatternListView(QWidget* parent)
    : QWidget(parent), ui(new Ui::PatternListView), patternModel(new PatternListModel(this)) {
  ui->setupUi(this);
  connect(ui->newButton, &QPushButton::clicked, this, [&]() {
    patternModel->insertRows(patternModel->rowCount(), 1);
  });
  connect(ui->insertButton, &QPushButton::clicked, this, [&]() {
    patternModel->insertRows(ui->list->currentIndex().row(), 1);
  });
  connect(ui->deleteButton, &QPushButton::clicked, this, [&]() {
    patternModel->removeRows(ui->list->currentIndex().row(), 1);
  });

  ui->list->setModel(patternModel);

  connect(ui->list->selectionModel(),
          &QItemSelectionModel::currentRowChanged,
          this,
          &PatternListView::currentPatternChanged);
}

void PatternListView::currentPatternChanged() {
  session->guiSession->setCurrentPattern(session->song.patterns[ui->list->currentIndex().row()].id);
}

// Here because of unique_ptr
PatternListView::~PatternListView() = default;

} // namespace gui
} // namespace qPs3
