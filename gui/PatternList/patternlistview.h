#pragma once

#include <QWidget>
#include <memory>

#include "core/sessionaware.h"

namespace Ui {
class PatternListView;
} // namespace Ui

namespace qPs3 {
namespace core {
class Song;
} // namespace core

namespace gui {

class PatternListModel;

class PatternListView : public QWidget, public core::SessionAware {
  Q_OBJECT
public:
  explicit PatternListView(QWidget* parent = nullptr);
  PatternListView(PatternListView&) = delete;
  PatternListView(PatternListView&&) = delete;
  PatternListView& operator=(PatternListView&) = delete;
  PatternListView& operator=(PatternListView&&) = delete;
  ~PatternListView() override;

private:
  void currentPatternChanged();

private:
  std::unique_ptr<Ui::PatternListView> ui;
  PatternListModel* patternModel;
};
} // namespace gui
} // namespace qPs3
