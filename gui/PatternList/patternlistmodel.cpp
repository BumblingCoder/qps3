#include "patternlistmodel.h"

#include "gui/guisession.h"
#include "gui/undocommands/addpatterncommand.h"
#include "gui/undocommands/genericdataundocommand.h"
#include "gui/undocommands/insertpatterncommand.h"
#include "gui/undocommands/removepatterncommand.h"

#include "core/session.h"
#include "core/song/song.h"

namespace qPs3 {
namespace gui {

PatternListModel::PatternListModel(QObject* parent) : QAbstractListModel(parent) {}

QVariant PatternListModel::headerData(int section, Qt::Orientation orientation, int role) const {
  if(section == 0 && orientation == Qt::Horizontal && role == Qt::DisplayRole) {
    return tr("Pattern Name");
  }
  return QVariant{};
}

int PatternListModel::rowCount(const QModelIndex& parent) const {
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if(parent.isValid()) { return 0; }
  if(session == nullptr) { return 0; }

  return static_cast<int>(session->song.patterns.size());
}

QVariant PatternListModel::data(const QModelIndex& index, int role) const {
  if(!index.isValid()) { return QVariant(); }
  if(role != Qt::DisplayRole && role != Qt::EditRole) { return QVariant{}; }

  return session->song.patterns[static_cast<size_t>(index.row())].name;
} // namespace gui

bool PatternListModel::setData(const QModelIndex& index, const QVariant& value, int role) {
  if(data(index, role) != value) {
    auto s = &session->song;
    session->guiSession->undoStack.push(new undo::GenericUndoCommand(
        [=]() -> core::Pattern& { return s->patterns[static_cast<size_t>(index.row())]; },
        &core::Pattern::name,
        value.toString(),
        this,
        index));
    emit dataChanged(index, index, QVector<int>() << Qt::DisplayRole << Qt::EditRole);
    return true;
  }
  return false;
}

Qt::ItemFlags PatternListModel::flags(const QModelIndex& index) const {
  if(!index.isValid()) { return Qt::NoItemFlags; }

  return Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

bool PatternListModel::insertRows(int row, int count, const QModelIndex& parent) {
  const bool atEnd = (rowCount() == row);
  beginInsertRows(parent, row, row + count - 1);
  for(int i = 0; i < count; ++i) {
    if(atEnd) {
      session->guiSession->undoStack.push(new undo::AddPatternCommand(*session));
    } else {
      session->guiSession->undoStack.push(new undo::InsertPatternCommand(session->song, row + i));
    }
  }
  endInsertRows();
  return true;
}

bool PatternListModel::removeRows(int row, int count, const QModelIndex& parent) {
  beginRemoveRows(parent, row, row + count - 1);
  for(int i = 0; i < count; ++i) {
    session->guiSession->undoStack.push(new undo::RemovePatternCommand(session->song, row + i));
  }
  endRemoveRows();
  return true;
}

void PatternListModel::sessionChanged() {
  beginResetModel();
  connect(session, &core::Session::songLoaded, this, [&]() {
    beginResetModel();
    endResetModel();
  });
  endResetModel();
}
} // namespace gui
} // namespace qPs3
