#pragma once

#include <QAbstractListModel>
#include <QUndoStack>

#include "core/sessionaware.h"

namespace qPs3 {
namespace core {
class Song;
} // namespace core

namespace gui {

class PatternListModel : public QAbstractListModel, public core::SessionAware {
  Q_OBJECT

public:
  explicit PatternListModel(QObject* parent = nullptr);

  // Header:
  QVariant
  headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  // Basic functionality:
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  // Editable:
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

  // Add data:
  bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;

  // Remove data:
  bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;

  void sessionChanged() override;
};
} // namespace gui
} // namespace qPs3
