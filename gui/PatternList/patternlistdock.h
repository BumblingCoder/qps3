#pragma once

#include <QDockWidget>

#include "core/sessionaware.h"

namespace Ui {
class PatternListDock;
}

namespace qPs3 {
namespace gui {

class PatternListDock : public QDockWidget, public core::SessionAware {
  Q_OBJECT

public:
  explicit PatternListDock(QWidget* parent = nullptr);
  PatternListDock(const PatternListDock&) = delete;
  PatternListDock(const PatternListDock&&) = delete;
  PatternListDock& operator=(const PatternListDock&) = delete;
  PatternListDock& operator=(const PatternListDock&&) = delete;
  ~PatternListDock() override;

private:
  Ui::PatternListDock* ui{};
};
} // namespace gui
} // namespace qPs3
