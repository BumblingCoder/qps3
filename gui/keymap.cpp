#include "keymap.h"

#include <QFile>
#include <QTextStream>

namespace qPs3 {
namespace gui {

KeyMap::KeyMap() {
  typingNoteMap[Qt::Key_QuoteLeft] = core::Note::NoteOff;
  typingNoteMap[Qt::Key_Z] = core::Note::C0;
  typingNoteMap[Qt::Key_S] = core::Note::Cs0;
  typingNoteMap[Qt::Key_X] = core::Note::D0;
  typingNoteMap[Qt::Key_D] = core::Note::Ds0;
  typingNoteMap[Qt::Key_C] = core::Note::E0;
  typingNoteMap[Qt::Key_V] = core::Note::F0;
  typingNoteMap[Qt::Key_G] = core::Note::Fs0;
  typingNoteMap[Qt::Key_B] = core::Note::G0;
  typingNoteMap[Qt::Key_H] = core::Note::Gs0;
  typingNoteMap[Qt::Key_N] = core::Note::A0;
  typingNoteMap[Qt::Key_J] = core::Note::As0;
  typingNoteMap[Qt::Key_M] = core::Note::B0;

  typingNoteMap[Qt::Key_Q] = core::Note::C1;
  typingNoteMap[Qt::Key_2] = core::Note::Cs1;
  typingNoteMap[Qt::Key_W] = core::Note::D1;
  typingNoteMap[Qt::Key_3] = core::Note::Ds1;
  typingNoteMap[Qt::Key_E] = core::Note::E1;
  typingNoteMap[Qt::Key_R] = core::Note::F1;
  typingNoteMap[Qt::Key_5] = core::Note::Fs1;
  typingNoteMap[Qt::Key_T] = core::Note::G1;
  typingNoteMap[Qt::Key_6] = core::Note::Gs1;
  typingNoteMap[Qt::Key_Y] = core::Note::A1;
  typingNoteMap[Qt::Key_7] = core::Note::As1;
  typingNoteMap[Qt::Key_U] = core::Note::B1;
  typingNoteMap[Qt::Key_I] = core::Note::C2;
  typingNoteMap[Qt::Key_9] = core::Note::Cs2;
  typingNoteMap[Qt::Key_O] = core::Note::D2;
  typingNoteMap[Qt::Key_0] = core::Note::Ds2;
  typingNoteMap[Qt::Key_P] = core::Note::E2;
}
static bool isBetween(int key, Qt::Key first, Qt::Key last) {
  return (first <= key && key <= last);
}

bool KeyMap::isNoteKey(int key) const { return typingNoteMap.find(key) != typingNoteMap.end(); }

core::Note KeyMap::noteForKey(int key) const { return typingNoteMap.find(key)->second; }

bool KeyMap::isHexKey(int key) const {
  if(isBetween(key, Qt::Key_0, Qt::Key_9)) { return true; }
  if(isBetween(key, Qt::Key_A, Qt::Key_F)) { return true; }
  return false;
}

int KeyMap::hexForKey(int key) const {
  if(isBetween(key, Qt::Key_0, Qt::Key_9)) { return key - Qt::Key_0; }
  if(isBetween(key, Qt::Key_A, Qt::Key_F)) { return key - Qt::Key_A + 0xa; }
  throw std::runtime_error("Tried to get hex digit for non-nex key");
}

bool KeyMap::isIntKey(int key) const { return isBetween(key, Qt::Key_0, Qt::Key_9); }

int KeyMap::intForKey(int key) const {
  if(isBetween(key, Qt::Key_0, Qt::Key_9)) { return key - Qt::Key_0; }
  throw std::runtime_error("Tried to get int digit for int key");
}

void KeyMap::loadTypingKeyMap(const QString& mapFile) {
  QFile file(mapFile);
  QTextStream ds(&file);
  for(auto item : typingNoteMap) {
    ds << item.first << " " << static_cast<int>(item.second) << '\n';
  }
}

void KeyMap::saveTypingKeyMap(const QString& mapFile) {
  QFile file(mapFile);
  QTextStream ds(&file);
  while(!ds.atEnd()) {
    int key;
    int note;
    ds >> key >> note;
    typingNoteMap[key] = static_cast<core::Note>(note);
  }
}

} // namespace gui
} // namespace qPs3
