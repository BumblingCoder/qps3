#pragma once

#include <QUndoCommand>

#include "core/song/song.h"

namespace qPs3 {
namespace core {
class Song;
}
namespace gui {
namespace undo {
class InsertPatternCommand : public QUndoCommand {
public:
  InsertPatternCommand(core::Song& s, int i);
  void undo() override;
  void redo() override;

  core::Song& song;
  const core::Pattern pattern;
  const int index;
};
} // namespace undo
} // namespace gui
} // namespace qPs3
