#include "insertnotecommand.h"

#include "core/song/patternfactory.h"
#include "core/song/song.h"

namespace qPs3 {
namespace gui {
namespace undo {

InsertNoteCommand::InsertNoteCommand(core::Song& s,
                                     utils::ID<core::Pattern> pat,
                                     QPoint c,
                                     core::Note n,
                                     uint8_t v,
                                     uint8_t d,
                                     uint8_t p)
    : song(s), pattern(pat), cell(c), note(n), volume(v), delay(d), pan(p) {
  setText(QObject::tr("Insert Note Pattern"));
}

void InsertNoteCommand::undo() {}

void InsertNoteCommand::redo() {}
} // namespace undo
} // namespace gui
} // namespace qPs3
