#include "insertpatterncommand.h"

#include "core/song/patternfactory.h"
#include "core/song/song.h"

namespace qPs3 {
namespace gui {
namespace undo {

InsertPatternCommand::InsertPatternCommand(core::Song& s, int i)
    : song(s), pattern(/*core::PatternFactory::getFactory().createPattern()*/), index(i) {
  setText(QObject::tr("Insert Pattern"));
}

void InsertPatternCommand::undo() { song.patterns.erase(song.patterns.begin() + index); }

void InsertPatternCommand::redo() { song.patterns.insert(song.patterns.begin() + index, pattern); }
} // namespace undo
} // namespace gui
} // namespace qPs3
