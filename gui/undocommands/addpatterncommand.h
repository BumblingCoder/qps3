#pragma once

#include <QUndoCommand>

#include "core/song/song.h"

namespace qPs3 {
namespace core {
class Session;
}
namespace gui {
namespace undo {
class AddPatternCommand : public QUndoCommand {
public:
  AddPatternCommand(core::Session& s);
  void undo() override;
  void redo() override;

  core::Session& session;
  core::Pattern pattern;
};
} // namespace undo
} // namespace gui
} // namespace qPs3
