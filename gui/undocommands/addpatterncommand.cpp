#include "addpatterncommand.h"

#include "core/session.h"
#include "core/song/patternfactory.h"
#include "core/song/song.h"

namespace qPs3 {
namespace gui {
namespace undo {

AddPatternCommand::AddPatternCommand(core::Session& s)
    : session(s), pattern(core::CreatePattern(session)) {
  setText(QObject::tr("Add Pattern"));
}

void AddPatternCommand::undo() { session.song.patterns.pop_back(); }

void AddPatternCommand::redo() { session.song.patterns.push_back(pattern); }
} // namespace undo
} // namespace gui
} // namespace qPs3
