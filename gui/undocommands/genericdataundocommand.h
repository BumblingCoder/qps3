#pragma once

#include <QUndoCommand>

#include <cassert>
#include <cstddef>
#include <utility>

namespace qPs3 {
namespace gui {
namespace undo {

template <class T, class O, class V, class I, class D>
class GenericUndoCommand : public QUndoCommand {
public:
  GenericUndoCommand(T containingObject, O oldValueRef, V newVal, I* issuer, D extraData)
      : QUndoCommand(nullptr)
      , containerFunction(containingObject)
      , memberPointer(oldValueRef)
      , issuingObject(issuer)
      , extra(extraData)
      , oldValue(containingObject().*oldValueRef)
      , newValue(std::move(newVal)) {
    static_assert(std::is_member_object_pointer<O>::value, "Requires a member object pointer");
    static_assert(std::is_reference<decltype(containingObject())>::value,
                  "T must return a reference so that we can change the data");
  }
  void undo() override { containerFunction().*memberPointer = oldValue; }
  void redo() override { containerFunction().*memberPointer = newValue; }

  T containerFunction;
  O memberPointer;
  I* issuingObject;
  D extra;

  V oldValue;
  V newValue;
};

template <class T, class O, class V, class I, class D>
GenericUndoCommand<T, O, V, I, D>* createGenericUndoCommand(T containingObject,
                                                            O oldValueRef,
                                                            const V& newVal,
                                                            I* issuer,
                                                            D extraData) {
  return new GenericUndoCommand<T, O, V, I, D>(
      containingObject, oldValueRef, newVal, issuer, extraData);
}
} // namespace undo
} // namespace gui
} // namespace qPs3
