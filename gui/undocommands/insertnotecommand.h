#pragma once

#include <QUndoCommand>

#include "core/song/song.h"

namespace qPs3 {
namespace core {
class Song;
}
namespace gui {
namespace undo {
class InsertNoteCommand : public QUndoCommand {
public:
  InsertNoteCommand(core::Song& s,
                    utils::ID<core::Pattern> pat,
                    QPoint c,
                    core::Note n,
                    uint8_t v,
                    uint8_t d,
                    uint8_t p);
  void undo() override;
  void redo() override;

  core::Song& song;
  const utils::ID<core::Pattern> pattern;
  const QPoint cell;
  const core::Note note;
  const uint8_t volume;
  const uint8_t delay;
  const uint8_t pan;
};
} // namespace undo
} // namespace gui
} // namespace qPs3
