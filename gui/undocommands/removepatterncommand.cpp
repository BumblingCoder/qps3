#include "removepatterncommand.h"

#include "core/song/patternfactory.h"
#include "core/song/song.h"

namespace qPs3 {
namespace gui {
namespace undo {

RemovePatternCommand::RemovePatternCommand(core::Song& s, int i)
    : song(s), pattern(song.patterns[i]), index(i) {
  setText(QObject::tr("Remove Pattern"));
}

void RemovePatternCommand::redo() { song.patterns.erase(song.patterns.begin() + index); }

void RemovePatternCommand::undo() { song.patterns.insert(song.patterns.begin() + index, pattern); }
} // namespace undo
} // namespace gui
} // namespace qPs3
