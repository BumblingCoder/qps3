import qbs

CppApplication {
    Depends {
        name: "tests"
    }
    Depends {
        name: "core"
    }
    Depends {
        name: "buildsettings"
    }
}
