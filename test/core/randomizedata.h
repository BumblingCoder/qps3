#pragma once

#include <random>
#include <utility>

#include <boost/hana.hpp>
#include <boost/hana/ext/boost/mpl.hpp>
#include <boost/variant.hpp>

#include "core/song/song.h"
#include "util/id.h"

class RandomizeData {
public:
  RandomizeData();

  template <class t> typename std::enable_if<std::is_enum<t>::value, void>::type randomize(t& n) {
    int a;
    randomize(a);
    n = static_cast<t>(a);
  }

  template <class t>
  typename std::enable_if<std::is_integral<t>::value, void>::type randomize(t& value) {
    std::uniform_int_distribution<t> dist(0);
    value = dist(randomEngine);
  }

  template <class t>
  typename std::enable_if<std::is_floating_point<t>::value, void>::type randomize(t& value) {
    std::uniform_real_distribution<double> dist(0);
    value = dist(randomEngine);
  }

  void randomize(QString& value) {
    int a;
    randomize(a);
    value = QString::number(a);
  }

  template <class t> void randomize(boost::optional<t>& value) {
    uint8_t a;
    randomize(a);
    if(a < 100) {
      t newVal;
      randomize(newVal);
      value = newVal;
    } else {
      value = boost::none;
    }
  }

  template <class t>
  typename std::enable_if<std::is_default_constructible<typename t::mapped_type>::value, void>::type
  randomize(t& value) {
    int range = std::uniform_int_distribution<int>(0, randomDataSize)(randomEngine);
    for(int i = 0; i < range; ++i) {
      typename t::key_type k;
      typename t::mapped_type v;
      randomize(k);
      randomize(v);
      value[k] = v;
    }
  }

  template <class t> void randomize(std::vector<t>& value) {
    int range = std::uniform_int_distribution<int>(0, randomDataSize)(randomEngine);
    for(int i = 0; i < range; ++i) {
      t v;
      randomize(v);
      value.push_back(v);
    }
  }

  template <class t> void randomize(qPs3::utils::ID<t>& value) { randomize(value._id); }

  template <class t>
  typename std::enable_if<t::reflectable::value, void>::type randomize(t& value) {
    boost::hana::for_each(boost::hana::keys(value), [&](const auto& key) {
      auto& subval = boost::hana::at_key(value, key);
      randomize(subval);
    });
  }

private:
  std::default_random_engine randomEngine;
  constexpr static const uint8_t randomDataSize = 3;
};
