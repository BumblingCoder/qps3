#include <gtest/gtest.h>

#include "core/song/song.h"
#include "randomizedata.h"
#include "util/hanaoperators.h"

RandomizeData::RandomizeData() : randomEngine(std::random_device()()) {}

class TestRandomizeData : public testing::Test {
protected:
  RandomizeData rd;
};

#define RandomTest(Type)                           \
  TEST_F(TestRandomizeData, TestRandomize##Type) { \
    Type i{};                                      \
    Type j{};                                      \
    rd.randomize(i);                               \
    EXPECT_NE(i, j);                               \
  }

using id = qPs3::utils::ID<TestRandomizeData>;
using namespace qPs3;
using namespace core;
RandomTest(int); // NOLINT
RandomTest(uint8_t); // NOLINT
RandomTest(double); // NOLINT
RandomTest(id); // NOLINT
RandomTest(NoteCell); // NOLINT
RandomTest(FxCell); // NOLINT
RandomTest(Track); // NOLINT
RandomTest(Pattern); // NOLINT
RandomTest(Song) // NOLINT
