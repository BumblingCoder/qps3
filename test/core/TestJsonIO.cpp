#include <gtest/gtest.h>

#include "core/fileio/jsonio.h"
#include "core/song/song.h"
#include "randomizedata.h"

using namespace qPs3;
using namespace core;

#define TestTypeIO(Type)                                                                   \
  TEST_F(TestJsonIO, TestWrite##Type) {                                                    \
    Type t1{};                                                                             \
    Type t2{};                                                                             \
    rd.randomize(t1);                                                                      \
    auto json = io::jsonValue(t1);                                                         \
    io::fromJson(t2, json);                                                                \
    EXPECT_EQ(t1, t2) << QJsonDocument(QJsonObject({{".", json}})).toJson().toStdString(); \
  }

#define TestStructIO(Type)                                                                 \
  TEST_F(TestJsonIO, TestWrite##Type) {                                                    \
    Type t1{};                                                                             \
    Type t2{};                                                                             \
    rd.randomize(t1);                                                                      \
    auto json = io::jsonValue(t1);                                                         \
    io::fromJson(t2, json);                                                                \
    boost::hana::for_each(boost::hana::keys(t1), [&](const auto& key) {                    \
      EXPECT_EQ(boost::hana::at_key(t1, key), boost::hana::at_key(t2, key))                \
          << boost::hana::to<const char*>(key);                                            \
    });                                                                                    \
    EXPECT_EQ(t1, t2) << QJsonDocument(QJsonObject({{".", json}})).toJson().toStdString(); \
  }
class TestJsonIO : public testing::Test {
protected:
  RandomizeData rd;
};

using VectorOfQStrings = std::vector<QString>;
using VectorOfPatterns = std::vector<Pattern>;

TestTypeIO(int); // NOLINT
TestTypeIO(uint8_t); // NOLINT
TestTypeIO(Note); // NOLINT
TestTypeIO(QString); // NOLINT
TestTypeIO(VectorOfQStrings); // NOLINT
TestTypeIO(VectorOfPatterns); // NOLINT
TestStructIO(NoteCell); // NOLINT
TestStructIO(FxCell); // NOLINT
TestStructIO(Track); // NOLINT
TestStructIO(Pattern); // NOLINT
TestStructIO(Song); // NOLINT
