#include <iostream>

#include <QApplication>
#include <QDebug>

#include "gui/mainwindow.h"

int main(int argc, char** argv) {
  QApplication app(argc, argv);
  app.setApplicationName("qPs3");
  app.setOrganizationName("qPs3");
  app.setApplicationVersion("0.0");

  // Install the QErrorMessage class' Qt message handler.
  //    QErrorMessage::qtHandler();

  // Try to load a localised translator.
  //    QTranslator translator;
  //    if (translator.load(QLocale::system().name(), app.applicationDirPath() +
  //    QLatin1String("/../i18n")))
  //        app.installTranslator(&translator);

  //  Instantiate the main window.
  qPs3::gui::MainWindow mainWindow;
  mainWindow.show();
  return app.exec();
}
