import qbs

Product {
    name: "util"
    Depends {
        name: "buildsettings"
    }
    Export {
        Depends {
            name: "cpp"
        }
        cpp.includePaths: [product.sourceDirectory]
    }
}
