#pragma once

#include <boost/hana.hpp>

#define QPSYHANACOMPARE                                                                     \
  template <class T> bool operator==(const T& other) const {                                \
    bool equal = true;                                                                      \
    boost::hana::for_each(boost::hana::keys(*this), [&](const auto& key) {                  \
      if(boost::hana::at_key(*this, key) != boost::hana::at_key(other, key)) equal = false; \
    });                                                                                     \
    return equal;                                                                           \
  }                                                                                         \
  template <class T> bool operator!=(const T& other) const { return !(*this == other); }
