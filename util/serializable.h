#pragma once

#include <QJsonValue>
namespace qPs3 {
namespace utils {

class Serializable {
public:
  virtual QJsonValue toJson() const = 0;
  virtual void fromJson(const QJsonValue&) = 0;
};

} // namespace utils
} // namespace qPs3
