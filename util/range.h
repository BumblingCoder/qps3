#pragma once

namespace qPs3 {
namespace utils {

struct range_iterator {
  range_iterator(int r) : _r(r) {}
  int operator*() { return _r; }
  range_iterator& operator++() {
    _r++;
    return *this;
  }
  bool operator==(const range_iterator& o) { return _r == o._r; }
  bool operator!=(const range_iterator& o) { return _r != o._r; }

private:
  int _r;
};

struct range {
  range(int r) : _r(r) {}
  range_iterator begin() { return range_iterator(0); }
  range_iterator end() { return range_iterator(_r); }

private:
  const int _r;
};

} // namespace utils
} // namespace qPs3
