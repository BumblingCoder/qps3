#pragma once

namespace qPs3 {
namespace utils {

struct ReflectionTag {
  static const bool value = true;
};

struct Reflectable {
  using reflectable = ReflectionTag;
};
} // namespace utils
} // namespace qPs3
