#pragma once

#include <functional>

namespace qPs3 {
namespace utils {

template <class T> class ID {
public:
  constexpr ID() = default;
  constexpr explicit ID(int id) : _id(id) {}
  ~ID() = default;
  constexpr ID(const ID& other) = default;
  constexpr ID(ID&& other) noexcept = default;
  ID& operator=(const ID& other) = default;
  ID& operator=(ID&& other) noexcept = default;
  bool operator<(const ID& other) const { return _id < other._id; }
  bool operator>(const ID& other) const { return _id > other._id; }
  bool operator==(const ID& other) const { return _id == other._id; }
  bool operator!=(const ID& other) const { return _id != other._id; }
  int _id = 0;
};
} // namespace utils
} // namespace qPs3

namespace std {
template <class T> struct hash<qPs3::utils::ID<T>> {
  using argument_type = qPs3::utils::ID<T>;
  using result_type = std::size_t;
  result_type operator()(argument_type const& s) const { return std::hash<int>{}(s._id); }
};

} // namespace std
