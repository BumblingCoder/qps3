#!/usr/bin/python3

import subprocess
import glob
import tempfile
import os

devnull = open(os.devnull, 'w')
dbdir = tempfile.TemporaryDirectory()
subprocess.check_output(
    ('qbs generate -g clangdb -d ' + dbdir.name).split(), stderr=devnull)
ccFile = glob.glob(dbdir.name + '/**/compile_commands.json', recursive=True)[0]
ccDir = os.path.dirname(ccFile)


def checkCppFile(file):
    output = subprocess.check_output(
        ('clang-tidy  -p ' + ccDir + ' ' + file).split(), universal_newlines=True, stderr=devnull)
    if 'warning:' in output:
        print(output)
        return False
    return True


if __name__ == "__main__":
    failingFiles = []
    filetypes = ['c', 'cpp']
    for type in filetypes:
        for file in glob.iglob('**/*.' + type, recursive=True):
            print(file)
            if not checkCppFile(file):
                failingFiles.append(file)

    if len(failingFiles) != 0:
        print(failingFiles)
        raise RuntimeError("Some files failed analysis")

    print("SUCCESS: No warnings detected!")
