import qbs

Project {
    minimumQbsVersion: "1.7.1"
    QtGuiApplication {
        consoleApplication: true
        files: "main.cpp"
        cpp.rpaths: "$ORIGIN"

        Group {
            // Properties for the produced executable
            fileTagsFilter: product.type
            qbs.install: true
        }
        Depends {
            name: "gui"
        }
        Depends {
            name: "core"
        }
        Depends {
            name: "util"
        }
        cpp.cxxLanguageVersion: "c++1z"
    }

    references: ["gui/gui.qbs", "core/core.qbs", "test/core/core-test.qbs", "util/util.qbs"]

    AutotestRunner {
    }

    Product {
        name: "Project Files"
        Group {
            name: "OtherFiles"
            files: ["*.py", ".clang-format", ".clang-tidy", ".gitlab-ci.yml"]
        }
    }

    qbsSearchPaths: "qbs"
}
