#!/usr/bin/python3

import subprocess
import glob

NoChangesXML = """<?xml version='1.0'?>
<replacements xml:space='preserve' incomplete_format='false'>
</replacements>
"""


def checkCppFile(file):
    output = subprocess.check_output(
        ("clang-format -output-replacements-xml " + file).split(), universal_newlines=True)
    if str(output) != NoChangesXML:
        return False
    return True


def fixCppFile(file):
    output = subprocess.check_output(
        ("clang-format -i " + file).split(), universal_newlines=True)
    return True


if __name__ == "__main__":
    failingFiles = []
    filetypes = ['c', 'cpp', 'h', 'hpp']
    for type in filetypes:
        for file in glob.iglob('**/*.' + type, recursive=True):
            if checkCppFile(file) == False:
                failingFiles.append(file)

    for file in glob.iglob('**/*.py', recursive=True):
        if len(subprocess.check_output(('autopep8 -d ' + file).split())) != 0:
            failingFiles.append(file)

    if len(failingFiles) != 0:
        print(failingFiles)
        raise RuntimeError("Some files were not formatted")

    print("SUCCESS: Code formatted correctly!")
